#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>

#include "errhandle.h"


#define LINES_COUNT                 10
#define PARENT_PREFIX               "[parent]"
#define CHILD_PREFIX                "[child]"

struct st_PrintArguments {
    char const *const prefix;
    sem_t *const p_wait_for;
    sem_t *const p_post_to;
};
typedef struct st_PrintArguments PrintArguments_t;

void *printLines(void *v_args) {
    if (NULL == v_args) {
        ferrorfln("Unexpected argument value: null");
        return NULL;
    }
    const PrintArguments_t *const args = (PrintArguments_t *) v_args;
    if (NULL == args->prefix) {
        ferrorfln("No prefix provided");
        return NULL;
    }
    if (NULL == args->p_post_to || NULL == args->p_wait_for) {
        ferrorfln("Both semaphores must be provided");
        return NULL;
    }

    sem_t *const p_wait_for = args->p_wait_for;
    sem_t *const p_post_to = args->p_post_to;

    for (int i = 0; i < LINES_COUNT; i += 1) {
        int errcode;
        do {
            errcode = sem_wait(p_wait_for);
            if (NO_ERROR != errcode && EINTR != errno) {
                errorfln("Failed to wait on semaphore: %s", strerror(errno));
                exit(EXIT_FAILURE);
            }
        } while (EINTR == errno);

        printf("%s line #%d\n", args->prefix, i);

        errcode = sem_post(p_post_to);
        if (NO_ERROR != errcode) { // EINVAL EOVERFLOW ENOSYS
            errorfln("Filed to post to semaphore: %s", strerror(errno));
            if (EOVERFLOW == errno) {
                return NULL;
            }
            exit(EXIT_FAILURE);
        }
    }

    return NULL;
}

int main() {
    int errcode;

    sem_t parent_sem;
    sem_t child_sem;

    sem_t *p_parent_sem = &parent_sem;
    sem_t *p_child_sem = &child_sem;

    errcode = sem_init(p_parent_sem, false, 1);
    if (NO_ERROR != errcode) {
        errorfln("Failed to init parent semaphore: %s", strerror(errcode));
        return EXIT_FAILURE;
    }
    errcode = sem_init(p_child_sem, false, 0);
    if (NO_ERROR != errcode) {
        errorfln("Failed to init child semaphore: %s", strerror(errcode));
        sem_destroy(&parent_sem);
        return EXIT_FAILURE;
    }

    pthread_t thread;
    errcode = pthread_create(&thread, NULL, printLines, &(PrintArguments_t) {
            .prefix = CHILD_PREFIX,
            .p_wait_for = p_child_sem,
            .p_post_to = p_parent_sem
    });
    if (NO_ERROR != errcode) {
        errorfln("Failed to spawn a thread: %s", strerror(errcode));
        return EXIT_FAILURE;
    }

    printLines(&(PrintArguments_t) {
            .prefix = PARENT_PREFIX,
            .p_wait_for = p_parent_sem,
            .p_post_to = p_child_sem
    });

    errcode = pthread_join(thread, NULL);
    if (NO_ERROR != errcode) {
        errorfln("Failed to join child thread: %s", strerror(errcode));
        return EXIT_FAILURE;
    }

    sem_destroy(&parent_sem);
    sem_destroy(&child_sem);

    return EXIT_SUCCESS;
}
